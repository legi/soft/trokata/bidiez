
# hosts = computer list to stop - mandatory
# temp_stop = upper temperature before stoping computer (x10 degree) - mandatory
# temp_start = lower temperature before accept starting computer (x10 degree) - mandatory
# attach = array of storage attach on computers
# wait_stop = tempo to wait after send halt command before stop attach storage (in second)
# wait_start = tempo to wait after poweron attach storage before poweron computer
# hosts_start = eventually smaller computer list to start (== hosts if not defined)
# theta = local temperature for this groupe of computer (== theta if not defined)

cron='yes'

connexion_db='/etc/bidiez/connexion.db'

# Gobal temperature get on UPS - mandatory
theta=$(snmpget -v1  -c public your-ups.example.com 1.3.6.1.4.1.4555.1.1.1.1.11.1 | awk '{print $4}')


# NAS server and SAN storage
temp_stop[0]=310  # max 31 degree
temp_start[0]=280
hosts[0]='hyper01 hyper02 hyper03 hyper04 hyper05'
attach[0]='san01 san02 san03 san04 san05'
wait_stop[0]=90
wait_start[0]=180

# Frontals cluster
temp_stop[1]=280  # max 28 degree
temp_start[1]=270
hosts[1]='frontal01 frontal02'

# Cluster nodes
temp_stop[2]=270  # max 27 degree
temp_start[2]=265
hosts[2]="$(seq --f "node%03.f" 1 16)"
hosts_start[2]="$(seq --f "node%03.f" 1 4)"
