# NAME

bidiez - start or stop computer based on temperature

# SYNOPSIS

```bash
bidiez [-v] [-h] [-f] start|stop|status|temp
```

# OPTIONS

    -f  force command and did not test temp
    -a  take the entire host list and not just reduced one (hosts_start)
    -v  verbose
    -h  help

# COMMAND

```bash
    bidiez [-v] [-f] [-a] start # start computers if temp < ref_value
    bidiez [-f] stop            # stop  computers if temp > ref_value
    bidiez status               # power status of computers
    bidiez temp                 # get actual temp
    bidiez halt                 # halt and poweroff computer
```

Command start and stop write bash command on STDOUT.
To execute it, pipe the result to bash!
Example:
```bash
bidiez stop | bash
```
There is absoloutly no danger to launch commands start or stop without
the final pipe...

The word bidiez means sheep in the Breton language.

# AUTHOR

Gabriel Moreau

# COPYRIGHT

Copyright (C) 2015-2021, LEGI UMR 5519 / CNRS UGA G-INP, Grenoble, France
Licence : GNU GPL version 2 or later
