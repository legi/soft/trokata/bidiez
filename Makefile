SHELL:=/bin/bash

DESTDIR=

BINDIR=/usr/bin
MANDIR=/usr/share/man/man1
SHAREDIR=/usr/share/bidiez
COMPDIR=/etc/bash_completion.d

.PHONY: all ignore install update sync upload stat help pkg pages

all:
	pod2man bidiez | gzip > bidiez.1.gz
	pod2html --css podstyle.css --index --header bidiez > bidiez.html

install: update

update:
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(SHAREDIR)
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(MANDIR)
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(COMPDIR)
	@install    -m 0755 -o root -g root bidiez $(DESTDIR)/$(BINDIR)
	@install    -m 0644 -o root -g root bidiez.1.gz $(DESTDIR)/$(MANDIR)
	@install    -m 0644 -o root -g root config.sample.yml $(DESTDIR)/$(SHAREDIR)
	@install    -m 0644 -o root -g root bidiez.bash_completion $(DESTDIR)/$(COMPDIR)/bidiez

sync:
	git pull

pkg: all
	./make-package-debian

pages: all pkg
	mkdir -p public/download
	cp -p *.html       public/
	cp -p podstyle.css public/
	cp -p LICENSE.txt  public/
	cp -p --no-clobber bidiez_*_all.deb  public/download/
	cd public; ln -sf bidiez.html index.html
	echo '<html><body><h1>DDT Debian Package</h1><ul>' > public/download/index.html
	(cd public/download; while read file; do printf '<li><a href="%s">%s</a> (%s)</li>\n' $$file $$file $$(stat -c %y $$file | cut -f 1 -d ' '); done < <(ls -1t *.deb) >> index.html)
	echo '</ul></body></html>' >> public/download/index.html

help:
	@echo "Possibles targets:"
	@echo " * all     : make manual"
	@echo " * install : complete install"
	@echo " * update  : update install (do not update cron file)"
	@echo " * sync    : sync with official repository"
	@echo " * pkg     : build Debian package"
