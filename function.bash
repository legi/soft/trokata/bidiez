
########################################################################
# Connexion database by default
########################################################################

export connexion_db=/etc/bidiez/connexion.db


########################################################################
# PDU host functions
########################################################################

function bidiez_host_poweron () {
   local computer=$1
   if [ -z "${computer}" -o "${computer}" == "-h" ]
   then
      echo "Usage: bidiez_host_poweron computer_name"
      return
   fi

   local pdus=$(grep "^${computer}\b" ${connexion_db} | awk '{print $2}')
   
   echo ${pdus} | sed -e 's/,/\n/g;s/:/ /g;' | while read pdu port
   do
      # continue if no port define
      [ -z "${port}" ] && continue
      
      # continue if outlet is already poweron (1)
      local outletstatus=$(snmpget -v1 -c public ${pdu}.hmg.priv  1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${port} | grep INTEGER | awk '{print $4}')
      [ "${outletstatus}" = "1" ] && continue
      
      # force poweron
      snmpset -v1 -c private ${pdu}.hmg.priv 1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${port} i 1 &
      sleep 1
   done
   }

function bidiez_host_poweroff () {
   local computer=$1
   if [ -z "${computer}" -o "${computer}" == "-h" ]
   then
      echo "Usage: bidiez_host_poweroff computer_name"
      return
   fi

   local pdus=$(grep "^${computer}\b" ${connexion_db} | awk '{print $2}')
   
   echo ${pdus} | sed -e 's/,/\n/g;s/:/ /g;' | while read pdu port
   do
      # continue if no port define
      [ -z "${port}" ] && continue
      
      # continue if outlet is already poweroff (2)
      local outletstatus=$(snmpget -v1 -c public ${pdu}.hmg.priv  1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${port} | grep INTEGER | awk '{print $4}')
      [ "${outletstatus}" = "2" ] && continue
      
      # force poweroff
      snmpset -v1 -c private ${pdu}.hmg.priv 1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${port} i 2 &
      sleep 1
   done
   }

function bidiez_host_powerstatus () {
   local computer=$1
   if [ -z "${computer}" -o "${computer}" == "-h" ]
   then
      echo "Usage: bidiez_host_powerstatus computer_name"
      echo "Return: on|off|partial|error"
      return
   fi

   local pdus=$(grep "^${computer}\b" ${connexion_db} | awk '{print $2}')
   local power_up=0
   local power_count=0
   
   while read pdu port
   do
      # continue if no port define
      [ -z "${port}" ] && continue
      
      # continue if outlet is already poweroff (2)
      local outlet_status=$(snmpget -v1 -c public ${pdu}.hmg.priv  1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${port} | grep INTEGER | awk '{print $4}')
      [ "${outlet_status}" = "1" ] && power_up=$((${power_up} + 1))
      power_count=$((${power_count} + 1))
   done < <(echo ${pdus} | sed -e 's/,/\n/g;s/:/ /g;')
   
   if [ ${power_up} -gt 0 -a ${power_count} -eq ${power_up} ]
   then
      echo 'on'
   elif [ ${power_up} -gt 0 -a ${power_count} -gt ${power_up} ]
   then
      echo 'partial'
   elif [ ${power_up} -eq 0 -a ${power_count} -gt 0 ]
   then
      echo 'off'
   else
      echo 'error'
   fi
   }

function bidiez_host_powerkwattph () {
   local computer=$1
   if [ -z "${computer}" -o "${computer}" == "-h" ]
   then
      echo "Usage: bidiez_host_powerkwattph computer_name"
      return
   fi

   local pdus=$(grep "^${computer}\b" ${connexion_db} | awk '{print $2}')
   local powerkwattph=0
   
   while read pdu port
   do
      # continue if no port define
      [ -z "${port}" ] && continue
      
      # continue if outlet is already poweroff (2)
      local outlet_kwattph=$(snmpget -v1 -c public ${pdu}.hmg.priv  1.3.6.1.4.1.318.1.1.26.9.4.3.1.11.${port} | grep INTEGER | awk '{print $4}')
      [[ "${outlet_kwattph}" =~ ^[[:digit:]]*$ ]] && powerkwattph=$((${powerkwattph} + ${outlet_kwattph}))
   done < <(echo ${pdus} | sed -e 's/,/\n/g;s/:/ /g;')
   
   echo ${powerkwattph}
   }

function bidiez_host_force_halt () {
   local computer=$1

   ssh ${computer} sudo /sbin/halt > /dev/null 2>&1 &
   sleep 80
   ipmitool -H ${computer}-bmc -U root -P root power off > /dev/null 2>&1 &
   sleep 5
   grep -q "^${computer}\b" ${connexion_db} && bidiez_host_poweroff ${computer}
   }


########################################################################
# PDU global functions
########################################################################

function bidiez_pdu_allon () {
   local pdu=$1
   if [ -z "${pdu}" -o "${pdu}" == "-h" ]
   then
      echo "Usage: bidiez_pdu_allon pdu_name"
      return
   fi

   for port in $(seq 1 24)
   do
      # continue if outlet is already poweron (1)
      local outletstatus=$(snmpget -v1 -c public ${pdu}.hmg.priv  1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${port} | grep INTEGER | awk '{print $4}')
      [ "${outletstatus}" = "1" ] && continue
      
      # force poweron
      snmpset -v1 -c private ${pdu}.hmg.priv 1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${port} i 1
   done
   }

function bidiez_pdu_alloff () {
   local pdu=$1
   if [ -z "${pdu}" -o "${pdu}" == "-h" ]
   then
      echo "Usage: bidiez_pdu_alloff pdu_name"
      return
   fi

   for port in $(seq 1 24)
   do
      # continue if outlet is already poweroff (2)
      local outletstatus=$(snmpget -v1 -c public ${pdu}.hmg.priv  1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${port} | grep INTEGER | awk '{print $4}')
      [ "${outletstatus}" = "2" ] && continue
      
      # force poweroff
      snmpset -v1 -c private ${pdu}.hmg.priv 1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${port} i 2
   done
   }


########################################################################
# PDU port functions
########################################################################

function bidiez_port_status () {
   local pdu=$1
   local port=$2
   if [ -z "${pdu}" -o -z "${port}" -o "${pdu}" == "-h" ]
   then
      echo "Usage: bidiez_port_status pdu_name port"
      return
   fi

   local outletstatus=$(snmpget -v1 -c public ${pdu}.hmg.priv  1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${port} | grep INTEGER | awk '{print $4}')
   [ "${outletstatus}" = "2" ] && echo "${pdu}:${port} down"
   [ "${outletstatus}" = "1" ] && echo "${pdu}:${port} up"
   }

function bidiez_port_up () {
   local pdu=$1
   local port=$2
   if [ -z "${pdu}" -o -z "${port}" -o "${pdu}" == "-h" ]
   then
      echo "Usage: bidiez_port_up pdu_name port"
      return
   fi

   snmpset -v1 -c private ${pdu}.hmg.priv 1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${port} i 1
   }

function bidiez_port_down () {
   local pdu=$1
   local port=$2
   if [ -z "${pdu}" -o -z "${port}" -o "${pdu}" == "-h" ]
   then
      echo "Usage: bidiez_port_down pdu_name port"
      return
   fi

   snmpset -v1 -c private ${pdu}.hmg.priv 1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${port} i 2
   }

function bidiez_port_ampere () {
   local pdu=$1
   local port=$2
   if [ -z "${pdu}" -o -z "${port}" -o "${pdu}" == "-h" ]
   then
      echo "Usage: bidiez_port_ampere pdu_name port"
      return
   fi

   local outlet_ampere=$(snmpget -v1 -c public ${pdu}.hmg.priv  1.3.6.1.4.1.318.1.1.26.9.4.3.1.6.${port} | grep INTEGER | awk '{print $4}')
   echo "${pdu}:${port} ${outlet_ampere}"
   }

function bidiez_port_watt () {
   local pdu=$1
   local port=$2
   if [ -z "${pdu}" -o -z "${port}" -o "${pdu}" == "-h" ]
   then
      echo "Usage: bidiez_port_watt pdu_name port"
      return
   fi

   local outlet_watt=$(snmpget -v1 -c public ${pdu}.hmg.priv  1.3.6.1.4.1.318.1.1.26.9.4.3.1.7.${port} | grep INTEGER | awk '{print $4}')
   echo "${pdu}:${port} ${outlet_watt}"
   }

function bidiez_port_kwattph () {
   local pdu=$1
   local port=$2
   if [ -z "${pdu}" -o -z "${port}" -o "${pdu}" == "-h" ]
   then
      echo "Usage: bidiez_port_kwattph pdu_name port"
      return
   fi

   local outlet_kwattph=$(snmpget -v1 -c public ${pdu}.hmg.priv  1.3.6.1.4.1.318.1.1.26.9.4.3.1.11.${port} | grep INTEGER | awk '{print $4}')
   echo "${pdu}:${port} ${outlet_kwattph}"
   }
